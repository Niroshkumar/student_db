from django.conf.urls import url, include
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'^$', views.get_data,name='get_data'),
    url(r'^submit_data/(?P<key>.*)/$', views.store_rfid, name='submit_data'),
    url(r'get_data',views.GetData.as_view(),name='get_students_data'),

]
